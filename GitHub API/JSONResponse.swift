//
//  JSONResponse.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation
import Decodable

struct JSONResponse {
    
    var totalCount: Int
    var items:      Array<Repository>
    
}

extension JSONResponse: Decodable {
    
    static func decode(_ json: Any) throws -> JSONResponse {
        return try JSONResponse(totalCount: json => "total_count",
                                items: [Repository].decode(json => "items"))
    }
    
}
