//
//  PullRequestsTableViewController.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit

class PullRequestsTableViewController: UITableViewController, NoPullRequestsDelegate, ServiceResponseDelegate {

    // MARK: - Variables
    private let worker = PullRequestWorker()
    private var pullRequests: [PullRequest] = []
    var repository: Repository?
    private var activityIndicator: UIActivityIndicatorView?
    private var selectedPullRequest: PullRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "\(repository?.name ?? "")"
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator?.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        
        worker.delegate = self
        worker.url = repository?.pullURL ?? ""
        loadPullRequests()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print("WARNING: didReceiveMemoryWarning CLASS: \(String(describing: type(of: self)))\n")
    }
    
    // MARK: - No Pull Requests Delegate
    
    func popView() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Load Pull Requests
    
    func loadPullRequests() {
        activityIndicator?.startAnimating()
        worker.loadPullRequests()
    }
    
    // MARK: - Service Response Delegate
    
    func updateTableView(_ response: Any) {
        activityIndicator?.stopAnimating()
        
        if let pulls = response as? [PullRequest] {
            if pulls.count == 0 {
                let NoPullRequestsView: NoPullRequestsView = .fromNib()
                NoPullRequestsView.frame = self.view.frame
                NoPullRequestsView.delegate = self
                self.view.addSubview(NoPullRequestsView)
                self.view.bringSubview(toFront: NoPullRequestsView)
            } else {
                self.pullRequests = pulls
                self.tableView.reloadData()
            }
        }
    }
    
    func displayAlert() {
        activityIndicator?.stopAnimating()
        self.okAlert(title: "Erro", message: "Não foi possível recuperar os Pull Requests. Por favor, tente novamente mais tarde.")
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "pullRequestCell", for: indexPath) as? PullRequestTableViewCell else { return UITableViewCell() }
        
        let pullRequest = pullRequests[indexPath.item]
        
        cell.pullRequestNameLabel.text = pullRequest.title
        cell.pullRequestBody.text = pullRequest.body
        cell.pullRequestOwnerName.text = pullRequest.author.name
        
        let imageURL = URL(string: pullRequest.author.avatarURL)
        cell.pullRequestOwnerImageView.kf.setImage(with: imageURL,
                                                   placeholder: #imageLiteral(resourceName: "GitHub Filled-100"),
                                                   options: nil,
                                                   progressBlock: nil,
                                                   completionHandler: nil)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPullRequest = pullRequests[indexPath.item]
        performSegue(withIdentifier: "webViewSegue", sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webViewSegue" {
            
            if let nextView = segue.destination as? WebViewController {
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Voltar",
                                                                        style: .plain,
                                                                        target: nil,
                                                                        action: nil)
                nextView.pullRequestURL = selectedPullRequest?.htmlURL
            }
            
        }
    }

}
