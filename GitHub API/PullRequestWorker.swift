//
//  PullRequestWorker.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation

class PullRequestWorker: NSObject {
    
    var delegate: ServiceResponseDelegate?
    var url: String = ""
    
    func loadPullRequests() {
        ServiceClient.sharedInstance.getPullRequests(url) { (success, pulls) in
            if success {
                self.delegate?.updateTableView(pulls)
            } else {
                self.delegate?.displayAlert()
            }
        }
    }
    
}
