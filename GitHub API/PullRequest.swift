//
//  PullRequest.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation
import Decodable

struct PullRequest {
    
    var title:     String
    var author:    User
    var createdAt: String
    var body:      String
    var htmlURL:   String
    
}

extension PullRequest: Decodable {
    
    static func decode(_ json: Any) throws -> PullRequest {
        return try PullRequest(title: json => "title",
                               author: User.decode(json => "user"),
                               createdAt: json => "created_at",
                               body: json => "body",
                               htmlURL: json => "html_url")
    }
    
}
