//
//  User.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation
import Decodable

struct User {
    
    var name:      String
    var avatarURL: String
    
}

extension User: Decodable {
    
    static func decode(_ json: Any) throws -> User {
        return try User(name: json => "login",
                        avatarURL: json => "avatar_url")
    }
    
}
