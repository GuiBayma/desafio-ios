//
//  PullRequestTableViewCell.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var pullRequestNameLabel: UILabel!
    @IBOutlet weak var pullRequestBody: UILabel!
    @IBOutlet weak var pullRequestOwnerImageView: UIImageView!
    @IBOutlet weak var pullRequestOwnerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
