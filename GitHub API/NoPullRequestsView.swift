//
//  NoPullRequestsView.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit

class NoPullRequestsView: UIView {
    
    // MARK: - Variables
    
    var delegate: NoPullRequestsDelegate?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    // MARK: - Back Button Pressed
    
    @IBAction func backPressed(_ sender: UIButton) {
        self.delegate?.popView()
    }
    
}
