//
//  InfoTableViewController.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 25/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit

class InfoTableViewController: UITableViewController {

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print("WARNING: didReceiveMemoryWarning CLASS: \(String(describing: type(of: self)))\n")
    }
    
    // MARK: - Return Button
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true) {}
    }
    
    // MARK: - Links
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        guard let buttonTitle = sender.titleLabel?.text else {
            return
        }
        
        var urlString = ""
        
        switch buttonTitle {
        case "Alamofire":
            urlString = "https://github.com/Alamofire/Alamofire"
            break
        case "Decodable":
            urlString = "https://github.com/Anviking/Decodable"
            break
        case "Kingfisher":
            urlString = "https://github.com/onevcat/Kingfisher"
            break
        case "Icons8":
            urlString = "https://icons8.com"
            break
        default: break
        }
        
        if let url = URL(string: urlString) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                if !success {
                    self.okAlert(title: "Erro", message: "Não foi possível abrir a página. Por favor, tente novamente mais tarde.")
                }
            })
        }
        
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

}
