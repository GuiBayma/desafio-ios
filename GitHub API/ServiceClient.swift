//
//  ServiceClient.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation
import Alamofire

class ServiceClient: NSObject {
    
    // Singleton
    static let sharedInstance = ServiceClient()
    
    override private init () {
        super.init()
    }
    
    /**
     Retorna os repositórios mais populares, de uma linguagem específica, do GitHub
     
     - Parameter language: linguagem dos reposiórios.
     - Returns: Array de objetos Repository.
     */
    func getRepositories(language: String, page: Int, _ completion:@escaping (_ success: Bool, _ repositories: [Repository])->()) {
        
        print("Buscando por repositórios (página: \(page))...")
        Alamofire.request("\(Constants.searchURL)?q=language:\(language)&sort=stars&page=\(page)").validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                print("Resposta: NÃO OK: response.result.isSuccess\n")
                completion(false,[])
                return
            }
            guard response.result.value != nil else {
                print("Resposta: NÃO OK: response.result.value\n")
                completion(false,[])
                return
            }
            
            do {
                let json = try JSONResponse.decode(response.result.value!)
                print("Resposta: OK\n")
                completion(true,json.items)
            } catch {
                print("Resposta: NÃO OK: JSON Parse: \(error)\n")
                completion(false,[])
            }
        }
    }
    
    /**
     Retorna todos os Pull Requests de um repositório
     
     - Parameter url: url dos pull requests.
     - Returns: Array de objetos PullRequest.
     */
    func getPullRequests(_ pullURL: String, _ completion:@escaping (_ success: Bool, _ requests: [PullRequest])->()) {
        
        print("Requisitando Pull Requests...")
        Alamofire.request(pullURL).validate().responseJSON { (response) in
            
            guard response.result.isSuccess else {
                print("Resposta: NÃO OK: response.result.isSuccess\n")
                completion(false,[])
                return
            }
            guard response.result.value != nil else {
                print("Resposta: NÃO OK: response.result.value\n")
                completion(false,[])
                return
            }
            
            do {
                let json = try [PullRequest].decode(response.result.value!)
                print("Resposta: OK\n")
                completion(true,json)
            } catch {
                print("Resposta: NÃO OK: JSON Parse: \(error)\n")
                completion(false,[])
            }
        }
    }
    
}
