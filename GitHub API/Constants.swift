//
//  Constants.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation

struct Constants {
    
    // Git Hub Search URL
    static let searchURL = "https://api.github.com/search/repositories"
    
    // Git Hub Pull Requests URL
    static let repoPullURL = "https://api.github.com/repos/"
    
    // Linguagem dos repositórios
    static let language = "Java"
    
    
}
