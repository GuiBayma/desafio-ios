//
//  Repository.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation
import Decodable

struct Repository {
    
    var id:              Int
    var name:            String
    var owner:           User
    var repoDescription: String?
    var starCount:       Int
    var forkCount:       Int
    
    var pullURL: String { return "\(Constants.repoPullURL)\(owner.name)/\(name)/pulls" }
    
}

extension Repository: Decodable {
    
    static func decode(_ json: Any) throws -> Repository {
        return try Repository(id: json => "id",
                              name: json => "name",
                              owner: User.decode(json => "owner"),
                              repoDescription: json => "description",
                              starCount: json => "stargazers_count",
                              forkCount: json => "forks_count")
    }
    
}
