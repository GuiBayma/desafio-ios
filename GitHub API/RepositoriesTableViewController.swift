//
//  RepositoriesTableViewController.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoriesTableViewController: UITableViewController, ServiceResponseDelegate {
    
    // MARK: - Variables
    private let worker = RepositoriesWorker()
    private var repositories: [Repository] = []
    private var selectedRepository: Repository?
    private var activityIndicator: UIActivityIndicatorView?
    private var repositoriesCount = 0
    private var isLoading = false

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Repositórios \(Constants.language)"
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator?.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        
        worker.delegate = self
        self.loadRepositories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print("WARNING: didReceiveMemoryWarning CLASS: \(String(describing: type(of: self)))\n")
    }
    
    // MARK: - Load Repositories
    
    func loadRepositories() {
        if isLoading {
            return
        }
        self.activityIndicator?.startAnimating()
        isLoading = true
        worker.loadRepositories()
    }
    
    // MARK: - Service Response Delegate
    
    func updateTableView(_ response: Any) {
        self.activityIndicator?.stopAnimating()
        self.isLoading = false
        
        if let repos = response as? [Repository] {
            self.repositoriesCount += repos.count
            self.repositories.append(contentsOf: repos)
            tableView.reloadData()
        }
    }
    
    func displayAlert() {
        self.activityIndicator?.stopAnimating()
        self.isLoading = false
        self.okAlert(title: "Erro", message: "Não foi possível recuperar os repositórios. Por favor, tente novamente mais tarde.")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "repositoryCell", for: indexPath) as? RepositoryTableViewCell else { return UITableViewCell() }

        let repository = repositories[indexPath.item]
        
        cell.repositoryNameLabel.text        = repository.name
        cell.ownerNameLabel.text             = repository.owner.name
        cell.repositoryDescriptionLabel.text = repository.repoDescription
        cell.forksCountLabel.text            = "\(repository.forkCount)"
        cell.starsCountLabel.text            = "\(repository.starCount)"
        
        let imageURL = URL(string: repository.owner.avatarURL)
        cell.ownerImageView.kf.setImage(with: imageURL,
                                        placeholder: #imageLiteral(resourceName: "GitHub Filled-100"),
                                        options: nil,
                                        progressBlock: nil,
                                        completionHandler: nil)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRepository = repositories[indexPath.item]
        performSegue(withIdentifier: "pullRequestsSegue", sender: self)
    }
    
    /*
     *  Carrega mais paginas ao chegar próximo ao final da table view.
     */
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /*
         *  Altere este valor para carregar mais páginas antes do final da table view.
         *  Quanto maior o numero, mais longe do final carregará.
         */
        let loadBefore = 10
        
        if indexPath.row > repositoriesCount - loadBefore {
            loadRepositories()
        }
    }
    
    // MARK: - Info Button
    
    @IBAction func infoPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "infoSegue", sender: self)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pullRequestsSegue" {
            
            if let nextView = segue.destination as? PullRequestsTableViewController {
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Voltar",
                                                                        style: .plain,
                                                                        target: nil,
                                                                        action: nil)
                nextView.repository = selectedRepository
            }
            
        }
    }

}
