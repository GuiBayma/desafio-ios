//
//  WebViewController.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    // MARK: - Outlets & Variables
    
    @IBOutlet weak var webView: UIWebView!
    
    var pullRequestURL: String?
    private var activityIndicator: UIActivityIndicatorView?
    
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator?.hidesWhenStopped = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator!)
        
        webView.delegate = self
        loadWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print("WARNING: didReceiveMemoryWarning CLASS: \(String(describing: type(of: self)))\n")
    }
    
    // MARK: - Load Web View
    
    func loadWebView() {
        guard pullRequestURL != nil else { return }
        
        activityIndicator?.startAnimating()
        
        if let url = URL(string: pullRequestURL!) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    // MARK: - Web View Delegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator?.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator?.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("ERRO: webView didFailLoadWithError CLASS: \(String(describing: type(of: self)))\n\(error)\n")
        self.okAlert(title: "Erro", message: "Não foi possível carregar a página. Por favor, tente novamente mais tarde.")
    }
    
    // MARK: - Buttons
    
    @IBAction func backPressed(_ sender: UIButton) {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    @IBAction func forwardPressed(_ sender: UIButton) {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
}
