//
//  RepositoriesWorker.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation

class RepositoriesWorker: NSObject {
    
    var delegate: ServiceResponseDelegate?
    private var page = 1
    
    func loadRepositories() {
        
        ServiceClient.sharedInstance.getRepositories(language: Constants.language, page: page) { (success, repos) in
            if success {
                self.page += 1
                self.delegate?.updateTableView(repos)
            } else {
                self.delegate?.displayAlert()
            }
        }
        
    }
    
}
