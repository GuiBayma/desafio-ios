//
//  Protocols.swift
//  GitHub API
//
//  Created by Guilherme Bayma on 24/05/17.
//  Copyright © 2017 Bayma. All rights reserved.
//

import Foundation

protocol NoPullRequestsDelegate {
    func popView()
}

protocol ServiceResponseDelegate {
    func updateTableView(_ response: Any)
    func displayAlert()
}
